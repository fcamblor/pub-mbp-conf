# README #

You can bootstrap this by executing following command :


```
#!bash

bash <(curl -u BITBUCKET_LOGIN:BITBUCKET_PWD -s https://bitbucket.org/fcamblor/pub-mbp-conf/raw/master/bashscripts/restoreMBP-bootstrap.sh)
```

This is an example repository allowing to demonstrate how to bootstrap a new MBP device from an initial Mackup Backup installation.
I deliberately removed some sensible (private) data I have in my real (non public) git repository, particularly :

- Chef configuration with my certificates, in ~/.chef/ directory
- My MAVEN configuration in : ~/.m2/settings-security.xml et ~/.m2/settings.xml
- My ngrok2 configuration in ~/.ngrok2/ngrok.yml
- My tunnelblick configuration in ~/Library/Application\ Support/Tunnelblick/Configurations/
- My Remote desktop configuration for vsphere, located into Backups/Serveur\ OVH\ vSphere.rdp
- My different Backuped configuration present in ~/Backups/Mackup/ directory

Note that the bootstrapping+restoration script will have 3 main steps :

- Bootstrapping step : some pre-requisite utilities such as xcode, homebrew, git, zsh, mackup etc.. will be *manually* installed through `restoreMBP-bootstrap.sh` script
  Once CLI tool will have been installed, iTerm2 with my fcamblor-agnoster oh-my-zsh theme will be installed as long as some OSX Hacks allowing to improve default OS X configuration options (see the `hackOSX.sh` script for the whole hack which are covered)
  In the end, you will be prompted to open `iTerm2` where restoration will continue
- When iTerm2 will be opened, execute manually the `bashScript/bashScripts/restoreMBP.sh` script in order to pursue MBP restoration.
  This script is intended to re-install the whole Rubygems / homebrew packages / npm global packages / tools that were installed in your previous installation (see `bashscripts/reinstall*.sh` scripts).
  Note that everytime you start a new iTerm2 window, your Rubygems / homebrew packages / npm global packages will be backuped into those `bashscripts/reinstall*.sh` scripts, allowing to automatically track new packages you would install over time.
  Thoses files are intended to be commited into your repository everytime you install some package (it is your responsibility to commit those files at the frequency you want)
