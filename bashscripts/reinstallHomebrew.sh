#!/bin/bash

# Manually installing brew cask
brew install caskroom/cask/brew-cask

# Allowing to install alternate cask versions for packages (like beta/eap)
brew tap caskroom/versions
brew tap homebrew/dupes

brewPackages="
android-sdk
ant
autoconf
autojump
automake
bash-completion
boost
cairo
camlp4
clasp
cmake
coreutils
cscope
csshx
docker
fontconfig
fping
freetype
gd
gdbm
gettext
git
glib
gnu-sed
gnupg
go
graphviz
gringo
groovy
heroku
heroku-toolbelt
htop-osx
httpie
imagemagick
jasper
jenv
jpeg
jsawk
libarchive
libffi
libgphoto2
libicns
libpng
libsvg
libsvg-cairo
libtiff
libtool
libusb
libusb-compat
libyaml
little-cms2
lynx
mackup
makedepend
maven
mtr
nano
ncurses
nmap
node
nspr
ocaml
opam
openssl
optipng
parallel
pcre
pidof
pixman
pkg-config
potrace
protobuf
python
python3
re2c
readline
rename
ruby
sane-backends
scons
spidermonkey
sqlite
svg2png
tree
unrar
wget
xz
zsh
"

brewCaskPackages="
adobe-reader
alfred
android-file-transfer
atom
bartender
caffeine
dash
dashlane
deezer
discord
disk-inventory-x
dockertoolbox
evernote
firefox
flycut
gimp
github-desktop
google-chrome
google-chrome-canary
google-drive
gpgtools
intellij-idea
intellij-idea-eap
iterm2
iterm2-beta
java
java6
java7
menumeters
microsoft-office-2011
mysqlworkbench
osxfuse
postman
sauce-connect
skitch
skype
slack
sourcetree
spectacle
sublime-text
tunnelblick
virtualbox
virtualbox-extension-pack
vlc
"

# Removing starting carriage return
brewCaskPackages="${brewCaskPackages:1:${#brewCaskPackages}-1}"
brewPackages="${brewPackages:1:${#brewPackages}-1}"
  
#Deactivated parallel installation of brew packages since it fails
#IFS=' '
#echo $brewCaskPackages | parallel -j 10 'brew cask install {}'
#echo $brewPackages | parallel -j 10 'brew install {}'
brew cask install $brewCaskPackages
brew install $brewPackages

