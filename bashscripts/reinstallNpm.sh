#!/bin/bash
npm install -g diff-so-fancy
npm install -g generator-typescript-library
npm install -g gifi
npm install -g git-recent
npm install -g grunt-cli
npm install -g keybase
npm install -g keybase-installer
npm install -g n_
npm install -g ngrok
npm install -g npm
npm install -g tldr
npm install -g yo
