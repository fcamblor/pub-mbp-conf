#!/bin/bash

BOOTSTRAP_RESTORE_DONE_FILE=~/.bootstrap-restoration-done
RESTORE_DONE_FILE=~/.restoration-done

if [[ -f $RESTORE_DONE_FILE && -f $BOOTSTRAP_RESTORE_DONE_FILE ]]; then
  # Skipping, nothing to do since restoration have already been done...
  echo Restoration already done !
else
  if [[ -f $BOOTSTRAP_RESTORE_DONE_FILE ]]; then
    ./reinstallGems.sh
    ./reinstallHomebrew.sh
    ./reinstallNpm.sh
    ./reinstallTools.sh

    echo "Installing opam for patdiff..."
    opam init && opam update && opam install patdiff
    wait
    touch $RESTORE_DONE_FILE
  else
    # Bootstrapping mode
    echo First time restoreMBP.sh script is called : entering bootstrap mode...
    ./restoreMBP-bootstrap.sh
  fi
fi


