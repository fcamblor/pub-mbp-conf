#!/bin/sh

# See http://neocontra.blogspot.fr/2013/02/public-service-announcement-patdiff-is.html
patdiff $2 $5 -alt-old a/$5 -alt-new b/$5 | cat

