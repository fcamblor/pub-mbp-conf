#!/bin/bash

# First, installing pre-reqs
# This script is intended to be launched in terminal, prior to anything before restoration
# Once executed, minimum configuration will have been installed onto the mac, that is to say :
# - Homebrew with minimum packages (zsh & iterm)
# - A hacked OS X config
#
# Further installation may be launched inside iterm instead of raw terminal...

echo Entering restoreMBP bootstrap mode...

echo Prior to anything, we need command line tools to be installed .. click 'install' button in the popup coming then press enter here once installed
xcode-select --install
echo "[Press enter to continue once XCode is installed]"
read _

# Installing Homebrew and basic packages
mkdir -p ~/tools/homebrew
curl https://codeload.github.com/Homebrew/homebrew/legacy.tar.gz/master > ~/tools/homebrew/homebrew.tar.gz
cd ~/tools/homebrew/ && tar -xvzf homebrew.tar.gz
mv $(ls | grep Homebrew-)/* ./
mv $(ls | grep Homebrew-)/.* ./
rmdir $(ls | grep Homebrew-)
cd -
export PATH=$PATH:~/tools/homebrew/bin
export HOMEBREW_CACHE=~/tools/homebrew-cache

echo Homebrew installed !.. Installing basic packages..
brew install zsh git mackup parallel

echo Replacing existing mac osx bundled zsh with the new one brought by homebrew
sudo -- sh -c "echo ~/tools/homebrew/bin/zsh >> /etc/shells"
chsh -s ~/tools/homebrew/bin/zsh

brew cask install --caskroom=~/tools/homebrew-cask iterm2 alfred google-chrome slack
echo "Now, I'm going to install oh-my-zsh... After installation, oh-my-zsh may be automatically launched, thus suspending current shell installation session. In that case, please, simply 'exit' the oh-my-zsh shell."
echo "[Press enter to continue]"
read _

# Installing oh-my-zsh
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
# Cloning zsh theme and installing it
## First, installing requirements :
git clone https://github.com/powerline/fonts.git /tmp/fonts && cd /tmp/fonts/ && ./install.sh && cd - && rm -Rf /tmp/fonts
echo "Powerline fonts have been installed successfully ! Please, edit your iterm font to one supported by Powerline (mine is 'Source code powerline') in order to have special chars for agnoster-fcamblor theme readable"
echo "[Press enter to continue]"
read _

if [ ! -f ~/bashscripts/restoreMBP.sh ]; then
  git clone https://fcamblor@bitbucket.org/fcamblor/pub-mbp-conf.git /tmp/mbp-conf/

  # Putting back backuped mackup files
  mkdir -p ~/Backups/
  mv /tmp/mbp-conf/Backups/Mackup/ ~/Backups/Mackup
  mv /tmp/mbp-conf/.mackup* ~/
  mackup restore

  # Putting back every files to ~/
  cp -RaX /tmp/mbp-conf/. ~/
  
  rm -Rf /tmp/mbp-conf

  sed -i '' 's/url = https:\/\/.*/url = git@bitbucket.org:fcamblor\/pub-mbp-conf.git/g' ~/.git/config
fi

## Then, installing agnoster-fcamblor theme
git clone https://github.com/fcamblor/oh-my-zsh-agnoster-fcamblor.git ~/.oh-my-zsh-agnoster-fcamblor
cd ~/.oh-my-zsh-agnoster-fcamblor && ./install && cd -
echo "Last but not least, you will have to import iterm colors by going to iTerm > Preferences > Profiles > Colors tab then click on 'Load presets' menu on the bottom and click on 'Import'"
echo "Then, you will need to select ~/.itermcolors/Solarized Dark.itermcolors file (click cmd+shift+. in open file dialog to display .* files)"
echo "[Press enter to continue]"
read _

# Hacking OSX configuration...
echo Hacking OSX configuration
~/bashscripts/hackOSX.sh

# Disabling bootstrap mode for further calls
touch ~/.bootstrap-restoration-done

echo Restoration bootstrap mode finished ! You can now continue restoration through ~/bashscripts/restoreMBP.sh from iterm2 !
echo "[Press enter to continue]"
read _

cecho "Killing some open applications in order to take effect." $red

find ~/Library/Application\ Support/Dock -name "*.db" -maxdepth 1 -delete
for app in "Activity Monitor" "Address Book" "Calendar" "Contacts" "cfprefsd" \
  "Dock" "Finder" "Mail" "Messages" "Safari" "SystemUIServer" \
  "Terminal" "Transmission"; do
  killall "${app}" > /dev/null 2>&1
done
