#!/bin/bash

targetSha1=$1
currentSha1=`git rev-parse HEAD`
previousSha1=`git rev-parse HEAD^`
currentBranchName=`git rev-parse --abbrev-ref HEAD`
sha1ToApply=

if [ "" == "$1" ]
then
  echo "Usage: tardive-amend <sha1WhichShouldBeAltered> [<sha1OfTheCommitToApply>]"
  exit 0
fi

if [ "" != "$2" ]
then
  sha1ToApply=$2
else
  sha1ToApply=$currentSha1
fi

# Ensuring there is no pending changes in index
currentStatus=`git status -s | wc -l`
currentIgnorableStatus=`git status -s | grep "??" | wc -l`
if [ "$currentStatus" != "$currentIgnorableStatus"  ]
then
  echo "Pending changes in index !"
  exit -1
fi

echo "No pending changes in index... processing..."

# Going to targetSha1 and creating a branch from there
echo "Creating temporary branch tardive-amend..."
git checkout $targetSha1 && git branch tardive-amend && git checkout tardive-amend || exit

# Cherry picking current commit and amending target commit with it
echo "Amending commit ... (git cherry-pick $sha1ToApply --no-commit && git commit --amend)"
(git cherry-pick $sha1ToApply --no-commit && git commit --amend) ||
  {
     echo "Failed to amend commit ! Please fix this problem then run following commands manually :" 
     echo "  git commit --amend"
     if [ "$targetSha1" != `git rev-parse $sha1ToApply` ]
     then
        echo "  git cherry-pick $targetSha1..$sha1ToApply^"
     fi
     if [ "$currentSha1" != "$sha1ToApply" ]
     then
        echo "  git cherry-pick $sha1ToApply..$currentSha1"
     fi
     echo "  git checkout $currentBranchName && git reset --hard tardive-amend && git branch -D tardive-amend"
     exit
  }

# Re-applying every commit through current branch name -1... only if there is something to apply
# (we could be in the special case where $sha1ToApply^ == $targetSha1 => in that case, cherry pick will
# be useless and we should avoid it !)
if [ "$targetSha1" != `git rev-parse $sha1ToApply^` ]
then
  echo "Rewinding commits ... (git cherry-pick $targetSha1..$sha1ToApply^)"
  git cherry-pick $targetSha1..$sha1ToApply^ || 
    {
       echo "Failed to rewind commits ! Please fix this problem then run following commands manually :"
       echo "  git cherry-pick --continue"
       if [ "$currentSha1" != "$sha1ToApply" ]
       then
          echo "  git cherry-pick $sha1ToApply..$currentSha1"
       fi
       echo "  git checkout $currentBranchName && git reset --hard tardive-amend && git branch -D tardive-amend"
       exit
    }
fi

# If sha1ToApply is not current sha1, we should cherry pick commits from sha1ToApply through currentSha1
if [ "$currentSha1" != "$sha1ToApply" ]
then
  echo "Rewinding commits after $sha1ToApply... (git cherry-pick $sha1ToApply..$currentSha1)"
  git cherry-pick $sha1ToApply..$currentSha1 || 
    {
       echo "Failed to rewind commits ! Please fix this problem then run following commands manually :"
       echo "  git cherry-pick --continue"
       echo "  git checkout $currentBranchName && git reset --hard tardive-amend && git branch -D tardive-amend"
       exit
    }
fi

# Updating current branch name reference
git checkout $currentBranchName && git reset --hard tardive-amend && git branch -D tardive-amend || exit

