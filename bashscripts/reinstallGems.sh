#!/bin/bash
gem install --user-install bigdecimal
gem install --user-install did_you_mean
gem install --user-install io-console
gem install --user-install json
gem install --user-install minitest
gem install --user-install net-telnet
gem install --user-install power_assert
gem install --user-install psych
gem install --user-install rake
gem install --user-install rdoc
gem install --user-install test-unit
