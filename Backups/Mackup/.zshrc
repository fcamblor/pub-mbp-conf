# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="agnoster-fcamblor"
#DEFAULT_USER=fcamblor
DEFAULT_USER=$USER

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

source $ZSH/oh-my-zsh.sh

# User configuration

export PATH="/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/opt/X11/bin"
# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

export LC_CTYPE="utf-8"
export HOMEBREW_CASK_OPTS="--caskroom=~/tools/homebrew-cask --no-binaries"
export HOMEBREW_CACHE=~/tools/homebrew-cache

# To fix brew doctor warnings...
export PATH=/usr/local/bin:$PATH
export PATH=/usr/local/sbin:$PATH
export PATH=$PATH:/usr/local/share/npm/bin:$HOME/.jenv/bin:~/bashscripts:~/tools/android/android-sdk-macosx/platform-tools:~/tools/android/android-sdk-macosx/tools:~/tools/dart/dart-sdk/bin
export PATH=~/tools/homebrew/bin:~/tools/homebrew/sbin:$PATH:~/tools/appengine/current/bin:~/.gem/ruby/2.0.0/bin
# Allowing to look into local node_modules for executables
export PATH="./node_modules/.bin:$PATH"
# The next line updates PATH for the Google Cloud SDK.
export PATH=~/tools/google-cloud-sdk/bin:$PATH

export NODE_PATH="/usr/local/lib/node:/usr/local/lib/node_modules:$HOME/tools/homebrew/lib/node_modules"
export NODE_PATH=$NODE_PATH:/usr/local/share/npm/lib/node_modules

alias biggestFiles='du -ma | grep "\\.[^/]" | sort -nr | head -n 20'
alias cpwd="pwd | tr -d '\n' | pbcopy"
alias weather='curl -4 wttr.in/Bordeaux'

alias cleards="find . -name '*.DS_Store' -type f -delete && notify 'ALL .DS_STORE FILES RECURSIVELY REMOVED'"

alias resetDNS='sudo killall -HUP mDNSResponder'

alias exportJavaDebug='export JAVA_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=8000 -Xnoagent -Djava.compiler=NONE"'
alias fakeSMTP='cd ~/tools/fakeSMTP && sudo java -jar fakeSMTP-1.6.jar'

# Git aliases
# Jumping to git roots
alias git-root='cd $(git rev-parse --show-toplevel)'
alias gs='git st'

alias chromePlayback='open -a "Google Chrome" --args --playback-mode'

function pyhttp(){ python -m SimpleHTTPServer $1 }
function cpLatestDlTo(){ cp "$HOME/Downloads/$(ls -t ~/Downloads | head -n 1)" $1 }
function mvLatestDlTo(){ mv "$HOME/Downloads/$(ls -t ~/Downloads | head -n 1)" $1 }
function rmLatestDl(){ rm "$HOME/Downloads/$(ls -t ~/Downloads | head -n 1)" }

function screenCastAndroid(){ adb shell screenrecord --verbose ./sdcard/Download/$1.mp4; }

function notify() {
  osascript -e 'display notification "" with title "'$1'"'
}

function killPort() {
  lsof -i TCP:$1 | grep LISTEN | awk '{print $2}' | xargs kill -9
}

function usedPort(){
  lsof -ni tcp:$1
}

function fixCrlf(){
  iconv -f ISO-8859-1 -t UTF-8 "$1" > "tmp$1";
  mv "tmp$1" "$1";
  dos2unix "$1";
}

function fixCrlf2(){
  for file in $(find . -type f  -depth 1)
  do
    if [ ${file:0:1} = "." ]
    then
#       echo "File $file"
      iconv -f ISO-8859-1 -t UTF-8 "${file:2}" > "tmp${file:2}";
      mv "tmp${file:2}" "${file:2}";
      dos2unix "${file:2}";
    fi
  done

  for dir in $(find . -type d -depth 1)
  do
    if [ ${dir:0:1} = "." ]
    then
      echo Going into $dir ...
      cd "$dir"
      fixCrlf2
      cd ..
    fi
  done
}

export LSCOLORS=cxfxcxdxbxegedabagacad

autoload bashcompinit
bashcompinit
# See http://jeroenjanssens.com/2013/08/16/quickly-navigate-your-filesystem-from-the-command-line.html
export MARKPATH=$HOME/.marks
function jump {
    cd -P "$MARKPATH/$1" 2>/dev/null || echo "No such mark: $1"
}
function mark {
    mkdir -p "$MARKPATH"; ln -s "$(pwd)" "$MARKPATH/$1"
}
function unmark {
    rm -i "$MARKPATH/$1"
}
function marks {
    \ls -l "$MARKPATH" | tail -n +2 | sed 's/  / /g' | cut -d' ' -f9- | awk -F ' -> ' '{printf "%-10s -> %s\n", $1, $2}'
}
_completemarks() {
  local curw=${COMP_WORDS[COMP_CWORD]}
  local wordlist=$(find $MARKPATH -type l -exec basename {} \;)
  COMPREPLY=($(compgen -W '${wordlist[@]}' -- "$curw"))
  return 0
}
complete -F _completemarks jump unmark

function mkcd() {
    mkdir -p "$1" && cd "$1"
}

function cdl() {
    cd "$1" && ls -a
}

alias latestDL='ls -t ~/Downloads | head -n 20'

# For GNU coreutils
PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
MANPATH="/usr/local/opt/coreutils/libexec/gnuman:$MANPATH"


# Doing some things once restoration have been done (=in 99% of cases)
if [[ -f ~/.restoration-done ]]; then
  eval "$(jenv init -)"

  # The next line updates PATH for the Google Cloud SDK.
  source ~/tools/google-cloud-sdk/path.zsh.inc

  # The next line enables bash completion for gcloud.
  source ~/tools/google-cloud-sdk/completion.zsh.inc

  # Autojump
  source ~/tools/homebrew/Cellar/autojump/current/share/autojump/autojump.zsh

  # Exporting every homebrew packages into ~/bashscripts/reinstallHomebrew.sh
  cat << 'EOF' > ~/bashscripts/reinstallHomebrew.sh
#!/bin/bash

# Manually installing brew cask
brew install caskroom/cask/brew-cask

# Allowing to install alternate cask versions for packages (like beta/eap)
brew tap caskroom/versions
brew tap homebrew/dupes

EOF
  
  # Installing cask packages first as some brew standard packages (like maven) require java cask package to be installed
  echo 'brewPackages="' >> ~/bashscripts/reinstallHomebrew.sh;
  for pck in $(brew list | grep -v brew-cask ); do echo "$pck" >> ~/bashscripts/reinstallHomebrew.sh; done;
  echo '"' >> ~/bashscripts/reinstallHomebrew.sh;

  echo >> ~/bashscripts/reinstallHomebrew.sh;

  echo 'brewCaskPackages="' >> ~/bashscripts/reinstallHomebrew.sh;
  for pck in $(brew cask list ); do echo "$pck" >> ~/bashscripts/reinstallHomebrew.sh; done;
  echo '"' >> ~/bashscripts/reinstallHomebrew.sh;

  # Installing brew packages using parallel
  cat << 'EOF' >> ~/bashscripts/reinstallHomebrew.sh

# Removing starting carriage return
brewCaskPackages="${brewCaskPackages:1:${#brewCaskPackages}-1}"
brewPackages="${brewPackages:1:${#brewPackages}-1}"
  
IFS=' '
echo $brewCaskPackages | parallel -j 10 'brew cask install {}'
echo $brewPackages | parallel -j 10 'brew install {}'

EOF

  echo "#!/bin/bash" > ~/bashscripts/reinstallNpm.sh
  for pck in $(ls ~/tools/homebrew/lib/node_modules/); do echo "npm install -g $pck" >> ~/bashscripts/reinstallNpm.sh; done;
  
  echo "#!/bin/bash" > ~/bashscripts/reinstallGems.sh
  for pck in $(gem list --no-versions --no-verbose); do echo "gem install --user-install $pck" >> ~/bashscripts/reinstallGems.sh; done;
fi

